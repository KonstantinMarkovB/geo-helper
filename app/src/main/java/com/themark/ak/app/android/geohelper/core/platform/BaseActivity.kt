package com.themark.ak.app.android.geohelper.core.platform

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.themark.ak.app.android.geohelper.AndroidApplication
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.di.component.ApplicationComponent
import com.themark.ak.app.android.geohelper.core.extentions.applicationContext
import com.themark.ak.app.android.geohelper.core.extentions.viewContainer
import javax.inject.Inject

/**
 * Base Activity class with helper methods for handling fragment transaction.
 *
 * @see AppCompatActivity
 */
abstract class BaseActivity : AppCompatActivity() {

    /**
     * Get a layout id for setting content view.
     */
    protected open fun layoutId() = R.layout.activity_location

    protected open fun themeId() = R.style.NoActionBar

    /**
     * Gets a [ApplicationComponent] for dependency injection.
     */
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (this.application as AndroidApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /**
     * Change current app theme from LauncherTheme to AppTheme.
     *
     * The LauncherTheme contains background screen to display
     * during the app launching.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(themeId())
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
    }

    /**
     * Adds a [Fragment] to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment The fragment to be added.
     */
    protected fun addFragment(containerViewId: Int = R.id.mainFragmentContainer, fragment: Fragment) {
        this.supportFragmentManager.beginTransaction()
            .add(containerViewId, fragment).commit()
    }

    internal fun notify(@StringRes message: Int) =
        Snackbar.make(viewContainer, message, Snackbar.LENGTH_LONG).show()

    internal fun notifyWithAction(@StringRes message: Int, @StringRes actionText: Int, action: () -> Any) {
        val snackBar = Snackbar.make(viewContainer, message, Snackbar.LENGTH_LONG)
        snackBar.setAction(actionText) { action.invoke() }
        snackBar.setActionTextColor(ContextCompat.getColor(applicationContext, R.color.textColor))
        snackBar.show()
    }

    /**
     * Show a dialog fragment.
     */
    protected fun showDialogFragment(dialogFragment: DialogFragment){
        val ft = this.supportFragmentManager.beginTransaction()
        val prev = this.supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        dialogFragment.show(ft, "dialog")
    }

    protected fun isPermissionGranted(permission: String) =
        (ContextCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED)

    protected fun sendPermissionsRequest(requestCode: Int, vararg permission: String){
        ActivityCompat.requestPermissions(this,
            permission,
            requestCode)
    }

    protected fun isGranted(grantResults: IntArray) = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
}