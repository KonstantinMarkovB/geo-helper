package com.themark.ak.app.android.geohelper.core.platform

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.SupportMapFragment
import com.themark.ak.app.android.geohelper.AndroidApplication
import com.themark.ak.app.android.geohelper.core.di.component.ApplicationComponent
import javax.inject.Inject

open class BaseSupportMapFragment : SupportMapFragment() {
    /**
     * Gets a [ApplicationComponent] for dependency injection.
     */
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

}