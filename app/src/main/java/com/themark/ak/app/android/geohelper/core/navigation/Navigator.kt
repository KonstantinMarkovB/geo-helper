package com.themark.ak.app.android.geohelper.core.navigation

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class used to navigate through the application
 */
@Singleton
class Navigator @Inject constructor(){

    /**
     * Goes to the book list screen.
     *
     * @param context A context needed to open the destiny activity.
     */
//    fun navigateToMap(context: Context) = context.startActivity(MapActivity.callingIntent(context))

}