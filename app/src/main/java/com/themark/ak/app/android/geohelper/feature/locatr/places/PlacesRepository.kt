package com.themark.ak.app.android.geohelper.feature.locatr.places

import android.util.Log
import com.themark.ak.app.android.geohelper.core.di.module.ResourceModule
import com.themark.ak.app.android.geohelper.core.exception.Failure
import com.themark.ak.app.android.geohelper.core.functional.Either
import com.themark.ak.app.android.geohelper.core.platform.NetworkHandler
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Named

interface PlacesRepository {

    fun getPlaces(name: String, location: String, radius: String): Either<Failure, List<Place>>


    class GooglePlaces @Inject constructor(
        private val networkHandler: NetworkHandler,
        private val service: GooglePlacesService,
        @Named(ResourceModule.GOOGLE_KEY) private val key: String
    ): PlacesRepository {

        override fun getPlaces(name: String, location: String, radius: String): Either<Failure, List<Place>> {
            when(networkHandler.isConnected){
                true -> return request(
                    service.places(name, location, radius, key),
                    { it.getPlaces() },
                    GooglePlacesAnswerEntry.empty()
                )
                false, null -> return Either.Left(Failure.ServerError)
            }
        }

        private fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Either.Right(transform(response.body() ?: default))
                    false -> Either.Left(Failure.ServerError)
                }
            } catch (exception: Throwable) {
                Log.e("TEST", exception.toString())
                Either.Left(Failure.ServerError)
            }
        }

    }
}