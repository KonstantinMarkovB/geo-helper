package com.themark.ak.app.android.geohelper.feature.locatr.search

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.extentions.inflate
import com.themark.ak.app.android.geohelper.feature.locatr.places.Place
import kotlinx.android.synthetic.main.row_place.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class SearchAdapter @Inject constructor(): RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    var collection : List<Place> by Delegates.observable(emptyList()){
        _, _, _-> notifyDataSetChanged()
    }

    var onClickListener: (Place)->Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.row_place))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(collection[position], onClickListener)


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(place: Place, onClickListener: (Place)->Unit ){
            itemView.setOnClickListener { onClickListener(place) }
            itemView.title.text = place.name
            itemView.subTitle.text = place.vicinity
        }
    }

}