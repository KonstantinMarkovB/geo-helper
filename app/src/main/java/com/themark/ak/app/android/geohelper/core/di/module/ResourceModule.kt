package com.themark.ak.app.android.geohelper.core.di.module

import android.content.Context
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.themark.ak.app.android.geohelper.R
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ResourceModule {

    companion object{
        const val GOOGLE_KEY = "GOOGLE_KEY"
        const val LOCATION_CURRENT = "LOCATION_CURRENT"
        const val MAP_MARGIN = "MAP_MARGIN"
    }

    @Provides @Singleton @Named(GOOGLE_KEY)
    fun provideKey(context: Context) : String = context.resources.getString(R.string.google_maps_key)

    @Provides @Singleton @Named(LOCATION_CURRENT)
    fun provideLocationCurrent(context: Context) : String =  context.resources.getString(R.string.location_current)

    @Provides @Singleton @Named(MAP_MARGIN)
    fun provideMapMargin(context: Context): Int = context.resources.getDimensionPixelSize(R.dimen.map_inset_margin)
}