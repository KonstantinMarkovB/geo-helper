package com.themark.ak.app.android.geohelper.feature.locatr.direction

import com.themark.ak.app.android.geohelper.feature.locatr.direction.Step.*

data class DirectionAnswerEntry(val routes: List<Route>) {

    companion object{
        fun empty() = DirectionAnswerEntry(emptyList())
    }

    fun toSteps() : List<Step> = routes[0].legs[0].steps.map { it.toStep() }


    data class Route(val bounds: BoundEntry, val legs: List<LegEntry>)

    data class BoundEntry(val northeast: LocationEntry,
                          val southwest: LocationEntry)

    data class LegEntry(val distance: DistanceEntry,
                        val duration: DurationEntry,
                        val end_address: String,
                        val end_location: LocationEntry,
                        val start_address: String,
                        val start_location: LocationEntry,
                        val steps: List<StepEntry>
                   )

    data class StepEntry constructor(
        val distance: DistanceEntry,
        val duration: DurationEntry,
        val end_location: LocationEntry,
        val html_instructions: String,
        val polyline: PolylineEntry,
        val start_location: LocationEntry,
        val travel_mode: String
    ) {

        fun toStep(): Step {
            return Step(
                distance.toDistance(),
                duration.toDuration(),
                end_location.toLocation(),
                html_instructions,
                polyline.toPolyline(),
                start_location.toLocation(),
                travel_mode
            )
        }
    }

    data class DistanceEntry(val text: String, val value: Int) {
        fun toDistance(): Distance = Distance(text, value)
    }

    data class DurationEntry(val text: String, val value: Int) {
        fun toDuration(): Duration = Duration(text, value)
    }

    data class LocationEntry(val lat: Double, val lng: Double) {
        fun toLocation(): Location = Location(lat, lng)
    }

    data class PolylineEntry(val points: String) {
        fun toPolyline(): Polyline = Polyline(points)
    }

}
