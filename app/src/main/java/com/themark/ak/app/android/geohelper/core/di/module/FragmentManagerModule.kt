package com.themark.ak.app.android.geohelper.core.di.module

import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides

@Module
class FragmentManagerModule(private val fragmentManager: FragmentManager) {

    @Provides fun provideFragmentManager() = fragmentManager
}