package com.themark.ak.app.android.geohelper.feature.locatr.places

import java.net.URI

data class Place(val point: Point, val icon: URI, val name: String, val vicinity: String) {

    data class Point(val lat: Double, val lng: Double)

}