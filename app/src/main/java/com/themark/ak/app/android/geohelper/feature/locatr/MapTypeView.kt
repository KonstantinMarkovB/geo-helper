package com.themark.ak.app.android.geohelper.feature.locatr

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class MapTypeView(val type: Int, @StringRes val titleId: Int, val icon: Drawable)