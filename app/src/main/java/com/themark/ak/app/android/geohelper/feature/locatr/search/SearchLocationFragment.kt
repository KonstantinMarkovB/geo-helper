package com.themark.ak.app.android.geohelper.feature.locatr.search

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethod
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.extentions.observe
import com.themark.ak.app.android.geohelper.core.extentions.viewModel
import com.themark.ak.app.android.geohelper.core.platform.BaseFragment
import com.themark.ak.app.android.geohelper.feature.locatr.Location
import com.themark.ak.app.android.geohelper.feature.locatr.LocationViewModel
import com.themark.ak.app.android.geohelper.feature.locatr.places.Place
import kotlinx.android.synthetic.main.fragment_search_location.*
import javax.inject.Inject

class SearchLocationFragment : BaseFragment() {

    companion object {
        fun newInstance() : SearchLocationFragment {
            return SearchLocationFragment()
        }
    }

    @Inject lateinit var searchAdapter: SearchAdapter

    lateinit var locationVM: LocationViewModel

    override fun layoutId(): Int = R.layout.fragment_search_location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appComponent.inject(this)

        activity?.let {
            locationVM = viewModel(viewModelFactory, it) {
                observe(places, ::handlePlacesLoaded)
                observe(searchDialogVisibility,::handleSearchDialogVisibility )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI(){
        backBtn.setOnClickListener {
            hideKeyBoard()
            locationVM.hideSearchDialog()
        }

        searchResultList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        searchResultList.adapter = searchAdapter

        searchAdapter.onClickListener = {
            hideKeyBoard()
            locationVM.findDirection(Location(it.point.lat, it.point.lng))
        }
    }

    private fun handleSearchDialogVisibility(visibility: Boolean?){
        searchText.setOnFocusChangeListener { view, b ->
            if(b) {
                showKeyBoard()
            }
        }

        searchText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                p0?.let {
                    locationVM.loadPlaces(p0.toString())
                }
            }

        })

        visibility?.let { if(it)  searchText.requestFocus() }
    }

    private fun showKeyBoard(){
        activity?.let {
            val imm = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(searchText, InputMethod.SHOW_EXPLICIT)
        }
    }

    private fun hideKeyBoard() {
        activity?.let {
            val imm = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(searchText.windowToken, 0)

        }
    }
    private fun handlePlacesLoaded(places: List<Place>?){
        places?.let { searchAdapter.collection = it }
    }

}