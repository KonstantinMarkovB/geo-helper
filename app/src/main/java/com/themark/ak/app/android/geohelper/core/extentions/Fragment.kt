package com.themark.ak.app.android.geohelper.core.extentions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.themark.ak.app.android.geohelper.core.platform.BaseActivity
import com.themark.ak.app.android.geohelper.core.platform.BaseFragment
import kotlinx.android.synthetic.main.activity_location.*

inline fun <reified T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    lifeCycleOwner: Fragment = this,
    body: T.() -> Unit): T
{
    val vm = ViewModelProviders.of(lifeCycleOwner, factory)[T::class.java]
    vm.body()
    return vm
}

inline fun <reified T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    lifeCycleOwner: FragmentActivity,
    body: T.() -> Unit = {}): T
{
    val vm = ViewModelProviders.of(lifeCycleOwner, factory)[T::class.java]
    vm.body()
    return vm
}

//val BaseFragment.viewContainer: View get() = (activity as BaseActivity).fragmentContainer.rootView

val BaseFragment.viewContainer: View get() {
    return if(coordinatorLayout != null) coordinatorLayout
    else (activity as BaseActivity).mainFragmentContainer.rootView
}

val BaseFragment.applicationContext: Context get() = activity?.applicationContext!!