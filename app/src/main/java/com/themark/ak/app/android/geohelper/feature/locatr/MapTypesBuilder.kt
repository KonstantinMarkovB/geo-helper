package com.themark.ak.app.android.geohelper.feature.locatr

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.themark.ak.app.android.geohelper.R
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MapTypesBuilder @Inject constructor(val context: Context) {

    private var imgRadius = 0

    init {
        imgRadius = context.resources.getDimension(R.dimen.imgRadius).toInt()
    }

    fun getList() : List<MapTypeView> {
        return listOf(
            MapTypeView(GoogleMap.MAP_TYPE_NORMAL, R.string.map_type_normal, getBitMap(R.mipmap.map_mod_default)),
            MapTypeView(GoogleMap.MAP_TYPE_HYBRID, R.string.map_type_hybrid, getBitMap(R.mipmap.map_mod_hybrid)),
            MapTypeView(GoogleMap.MAP_TYPE_TERRAIN, R.string.map_type_terrain, getBitMap(R.mipmap.map_mod_terrain))
        )
    }

    private fun getBitMap (resId: Int): Drawable = ContextCompat.getDrawable(context, resId)!!
}