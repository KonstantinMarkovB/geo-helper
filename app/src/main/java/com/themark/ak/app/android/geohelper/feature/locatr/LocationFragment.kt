package com.themark.ak.app.android.geohelper.feature.locatr

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.extentions.viewModel
import com.themark.ak.app.android.geohelper.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_show_search_btn.*

class LocationFragment : BaseFragment() {

    companion object{
        fun newInstance(): Fragment{
            return LocationFragment()
        }
    }

    private lateinit var locationVM: LocationViewModel


    override fun layoutId() = R.layout.fragment_show_search_btn

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        appComponent.inject(this)

        activity?.let {
            locationVM = viewModel(viewModelFactory, it){ }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dist = Location(41.43206, -81.38992)
        find_btn.setOnClickListener {
            locationVM.showSearchDialog()
        }
    }



}