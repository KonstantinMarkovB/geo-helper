package com.themark.ak.app.android.geohelper.feature.locatr.places

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface GooglePlacesApi {

    companion object {
        private const val PARAM_NAME = "name"
        private const val PARAM_LOCATION = "location"
        private const val PARAM_RADIUS = "radius"
        private const val PARAM_KEY = "key"

        private const val DIR_URI = "json"
    }

    @GET(DIR_URI) fun places(@Query(
        PARAM_NAME
    ) name: String,
                             @Query(
                                                                                                                      PARAM_LOCATION
                                                                                                                  ) location: String,
                             @Query(
                                                                                                                      PARAM_RADIUS
                                                                                                                  ) radius: String,
                             @Query(
                                                                                                                      PARAM_KEY
                                                                                                                  ) key: String)
            : Call<GooglePlacesAnswerEntry>
}