package com.themark.ak.app.android.geohelper.feature.locatr.places

import java.net.URI

class GooglePlacesAnswerEntry(val results: List<Result>) {

    companion object {
        fun empty(): GooglePlacesAnswerEntry {
            return GooglePlacesAnswerEntry(emptyList())
        }
    }

    fun getPlaces(): List<Place> {
        return results.map {
            it.toPlace()
        }
    }

    data class Result(val geometry: Geometry,
                      val icon: String,
                      val id: String,
                      val name: String,
                      val vicinity: String){
        fun toPlace(): Place{
            return Place(
                Place.Point(geometry.location.lat, geometry.location.lng),
                URI(icon), name, vicinity)
        }
    }

    data class Geometry(val location: PointEntry, val viewport: ViewPortEntry)

    data class ViewPortEntry(val northeast: PointEntry, val southwest: PointEntry)

    data class PointEntry(val lat: Double, val lng: Double)

}