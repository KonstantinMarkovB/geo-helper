package com.themark.ak.app.android.geohelper.feature.locatr.direction

import android.util.Log
import com.themark.ak.app.android.geohelper.core.di.module.ResourceModule
import com.themark.ak.app.android.geohelper.core.exception.Failure
import com.themark.ak.app.android.geohelper.core.functional.Either
import com.themark.ak.app.android.geohelper.core.platform.NetworkHandler
import com.themark.ak.app.android.geohelper.feature.locatr.Location
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Named

interface DirectionRepository {

    fun getDirection(origin: Location, destination: Location): Either<Failure, List<Step>>


    class Google @Inject constructor(
        private val networkHandler: NetworkHandler,
        private val service: GoogleDirectionService,
        @Named(ResourceModule.GOOGLE_KEY) private val key: String
    ) : DirectionRepository {


        override fun getDirection(origin: Location, destination: Location): Either<Failure, List<Step>> {
            return when (networkHandler.isConnected){
                true ->  request(
                    service.destination(origin.toString(), destination.toString(), key),
                    { it.toSteps() } ,
                    DirectionAnswerEntry.empty()
                )
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }

        private fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Either.Right(transform(response.body() ?: default))
                    false -> Either.Left(Failure.ServerError)
                }
            } catch (exception: Throwable) {
                Log.e("TEST", exception.toString())
                Either.Left(Failure.ServerError)
            }
        }

    }

}