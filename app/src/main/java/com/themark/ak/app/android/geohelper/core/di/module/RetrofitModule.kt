package com.themark.ak.app.android.geohelper.core.di.module

import android.util.Log
import com.themark.ak.app.android.geohelper.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class RetrofitModule {

    companion object {
        const val DIRECTIONS = "directions"
        const val PLACES = "places"
        private const val DIRECTIONS_URL = "https://maps.googleapis.com/maps/api/directions/"
        private const val PLACES_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/"
    }

    @Provides
    @Singleton
    @Named(DIRECTIONS)
    fun provideDictionaryRetrofit(): Retrofit = provideRetrofit(DIRECTIONS_URL)

    @Provides
    @Singleton
    @Named(PLACES)
    fun providePlacesRetrofit(): Retrofit = provideRetrofit(PLACES_URL)

    private fun provideRetrofit(baseUrl: String):  Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(createClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createClient(): OkHttpClient {
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG){
            val httpLoggingInterceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Log.d("OkHttp", message)
                }
            })
            val loggingInterceptor =
                httpLoggingInterceptor.apply { level = HttpLoggingInterceptor.Level.BASIC }

            okHttpClientBuilder.addInterceptor(loggingInterceptor)
        }
        return okHttpClientBuilder.build()
    }
}