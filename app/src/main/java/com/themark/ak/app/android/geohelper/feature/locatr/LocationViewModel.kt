package com.themark.ak.app.android.geohelper.feature.locatr

import android.content.Context
import android.location.Location
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.di.module.ResourceModule
import com.themark.ak.app.android.geohelper.core.platform.BaseViewModel
import com.themark.ak.app.android.geohelper.feature.locatr.direction.GetDirection
import com.themark.ak.app.android.geohelper.feature.locatr.direction.Step
import com.themark.ak.app.android.geohelper.feature.locatr.places.GetPlaces
import com.themark.ak.app.android.geohelper.feature.locatr.places.Place
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

class LocationViewModel @Inject constructor(
    private val context: Context,
    private val mapTypesBuilder: MapTypesBuilder,
    private val getDirection: GetDirection,
    private val getPlaces: GetPlaces,
    @Named(ResourceModule.LOCATION_CURRENT) private val locationCurrent: String,
    @Named(ResourceModule.MAP_MARGIN) private val margin: Int
): BaseViewModel() {

    init {
        getDirection.scope = viewModelScope
        getPlaces.scope = viewModelScope
    }

    private val zoom = 17f
    private val placesRadius = 3000

    private val _mapTypes = MutableLiveData<List<MapTypeView>>().apply { value = mapTypesBuilder.getList() }
    val mapTypes: LiveData<List<MapTypeView>> = _mapTypes

    private val _mapTypesVisibility = MutableLiveData<Boolean>()
    val mapTypeVisibility: LiveData<Boolean> = _mapTypesVisibility

    private val _isConnected = MutableLiveData<Boolean>().apply { value = false }
    val isConnected: LiveData<Boolean> = _isConnected

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isMapLoaded = MutableLiveData<Boolean>()
    val isMapLoaded: LiveData<Boolean> = _isMapLoaded

    private val _places = MutableLiveData<List<Place>>()
    val places: LiveData<List<Place>> = _places

    private val _isPlacesLoaded = MutableLiveData<Boolean>()
    val isPlacesLoaded: LiveData<Boolean> = _isPlacesLoaded

    private val _searchDialogVisibility = MutableLiveData<Boolean>()
    val searchDialogVisibility: LiveData<Boolean> = _searchDialogVisibility

    private var _hasDirection = false

    private val client = buildClient()
    private var map: GoogleMap? = null

    lateinit var currentPointer: BitmapDescriptor
    lateinit var betweenPointer: BitmapDescriptor

    fun setMap(map: GoogleMap){
        this.map = map

        currentPointer = BitmapDescriptorFactory.fromResource(R.mipmap.pointer)
        betweenPointer = BitmapDescriptorFactory.fromResource(R.mipmap.between_point)

        _isMapLoaded.value = true
    }

    fun showCurrentLocation() = getCurrentLocation(::moveToCurrentPos)

    fun showSearchDialog(){
        _searchDialogVisibility.value = true
    }

    fun hideSearchDialog(){
        _searchDialogVisibility.value = false
    }

    fun loadPlaces(text: String){
        _isPlacesLoaded.value = false

        getCurrentLocation ({
            val loc = "${it.latitude},${it.longitude}"
            getPlaces(GetPlaces.Params(text, loc, placesRadius.toString())){
                it.either(::handleFailure, ::handlePlacesLoaded)}
        })
    }

    private fun handlePlacesLoaded(places: List<Place>?){
        _isPlacesLoaded.value = true

        places?.let {
            _places.value = places
        }
    }

    fun findDirection(destination: com.themark.ak.app.android.geohelper.feature.locatr.Location){
        _searchDialogVisibility.value = false

        getCurrentLocation ({
            val origin = Location(it.latitude, it.longitude)

            getDirection(GetDirection.Params(origin, destination)){
                it.either(::handleFailure, ::handleDirSuccess)
            }
        })
    }

    fun handleDirSuccess(list: List<Step>?){
        list?.let {
            viewModelScope.launch {
                _hasDirection = true
                map?.clear()

                val bounds = LatLngBounds.Builder()
                val polyline =  PolylineOptions()
                val firstPoint = it[0]
                val lastPoint = it[it.lastIndex]

                var latLng = LatLng(firstPoint.startLocation.lat, firstPoint.startLocation.lng)
                bounds.include(latLng)
                polyline.add(latLng)

                for(i in 1 .. it.lastIndex){
                    val st = it[i]
                    val latLng = LatLng(st.startLocation.lat, st.startLocation.lng)

                    bounds.include(latLng)
                    polyline.add(latLng)

                    map?.addMarker(MarkerOptions()
                        .position(latLng)
                        .title(st.htmlInstructions)
                        .snippet(st.duration.text)
                        .icon(betweenPointer))
                }


                latLng = LatLng(lastPoint.endLocation.lat, lastPoint.endLocation.lng)
                bounds.include(latLng)
                polyline.add(latLng)

                map?.addMarker(MarkerOptions()
                    .position(latLng)
                    .title(lastPoint.htmlInstructions).snippet(lastPoint.distance.text))


                latLng = LatLng(firstPoint.startLocation.lat, firstPoint.startLocation.lng)
                map?.addMarker(MarkerOptions()
                    .position(latLng)
                    .title(firstPoint.htmlInstructions).snippet(firstPoint.distance.text)
                    .icon(currentPointer))

                map?.addPolyline(polyline)

                map?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), margin))
            }.start()
        }
    }

    private fun getCurrentLocation(callback: (Location) -> Unit, failureCallback: ()-> Unit = {}){
        _isLoading.value = true

        LocationServices.getFusedLocationProviderClient(context)
            .lastLocation.addOnSuccessListener {
            _isLoading.value = false

            if(it != null){
                callback(it)
            } else {
                failure.value = LocationDefinitionFailure()
                failureCallback()
            }
        }
    }

    private fun moveToCurrentPos(location: Location){
        val myPoint = LatLng(location.latitude, location.longitude)
        val zoom = CameraUpdateFactory.newLatLngZoom(myPoint, zoom)

        if(!_hasDirection) {
            map?.clear()

            map?.addMarker(
                MarkerOptions()
                    .position(myPoint)
                    .title(locationCurrent)
                    .snippet("{${myPoint.latitude},${myPoint.longitude}}")
                    .icon(currentPointer)
            )
        }

        map?.animateCamera(zoom)
    }

    fun changeMapType(type: MapTypeView){
        map?.mapType = type.type
        hideMapTypes()
    }

    fun showMapTypes(){
        _mapTypesVisibility.value = true
    }

    fun hideMapTypes(){
        _mapTypesVisibility.value = false
    }

    fun onStart(){
        client.connect()
        _isConnected.value = true
    }

    fun onStop(){
        client.disconnect()
        _isConnected.value = false
    }

    private fun buildClient() =
        GoogleApiClient.Builder(context)
        .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks{
            override fun onConnected(p0: Bundle?) {
                _isConnected.value = true
            }

            override fun onConnectionSuspended(p0: Int) {}
        })
        .addApi(LocationServices.API)
        .build()
}