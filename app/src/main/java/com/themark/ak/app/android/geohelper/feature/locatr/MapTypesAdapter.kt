package com.themark.ak.app.android.geohelper.feature.locatr

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.extentions.inflate
import kotlinx.android.synthetic.main.row_map_type.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class MapTypesAdapter @Inject constructor()
    : RecyclerView.Adapter<MapTypesAdapter.ViewHolder>() {

    internal var collection: List<MapTypeView> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    internal var clickListener: (MapTypeView) -> Unit = {_ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.row_map_type))

    override fun getItemCount(): Int =
        collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(collection[position], clickListener)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mapTypeView: MapTypeView, clickListener: (MapTypeView) -> Unit ){
            itemView.icon.background = mapTypeView.icon
            itemView.text.setText(mapTypeView.titleId)
            itemView.setOnClickListener { clickListener(mapTypeView) }
        }
    }
}