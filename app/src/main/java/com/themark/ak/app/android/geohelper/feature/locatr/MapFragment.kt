package com.themark.ak.app.android.geohelper.feature.locatr

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.themark.ak.app.android.geohelper.core.extentions.viewModel
import com.themark.ak.app.android.geohelper.core.platform.BaseSupportMapFragment

class MapFragment : BaseSupportMapFragment() {

    companion object {
        fun newInstance(): Fragment {
            return MapFragment()
        }
    }

    lateinit var locationVM: LocationViewModel

    override fun onCreate(p0: Bundle?) {
        super.onCreate(p0)
        appComponent.inject(this)

        activity?.let { it ->
            locationVM = viewModel(viewModelFactory, it){}

            getMapAsync { locationVM.setMap(it) }
        }
    }

}