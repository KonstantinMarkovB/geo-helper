/**
 * Copyright (C) 2018 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.themark.ak.app.android.geohelper.core.di.module

import android.content.Context
import com.themark.ak.app.android.geohelper.AndroidApplication
import com.themark.ak.app.android.geohelper.feature.locatr.direction.DirectionRepository
import com.themark.ak.app.android.geohelper.feature.locatr.places.PlacesRepository

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides @Singleton fun provideApplicationContext(): Context = application

    @Provides @Singleton fun provideDirectionRepository(repository: DirectionRepository.Google)
            : DirectionRepository = repository

    @Provides @Singleton fun providePlacesRepository(repository: PlacesRepository.GooglePlaces)
            : PlacesRepository = repository

}

