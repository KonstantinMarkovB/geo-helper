package com.themark.ak.app.android.geohelper.feature.locatr

class Location(val lat: Double, val lng: Double){
    override fun toString(): String = "$lat,$lng"
}