package com.themark.ak.app.android.geohelper.feature.locatr.direction

import com.themark.ak.app.android.geohelper.core.exception.Failure
import com.themark.ak.app.android.geohelper.core.functional.Either
import com.themark.ak.app.android.geohelper.core.interactor.UseCase
import com.themark.ak.app.android.geohelper.feature.locatr.Location
import javax.inject.Inject

class GetDirection @Inject constructor(private val directionRepository: DirectionRepository)
    : UseCase<List<Step>, GetDirection.Params>() {

    override suspend fun run(params: Params): Either<Failure, List<Step>> =
        directionRepository.getDirection(params.origin, params.destination)

    class Params(val origin: Location, val destination: Location)
}