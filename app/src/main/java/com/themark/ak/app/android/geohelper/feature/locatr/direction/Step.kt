package com.themark.ak.app.android.geohelper.feature.locatr.direction

data class Step constructor (val distance: Distance,
                           val duration: Duration,
                           val endLocation: Location,
                           val htmlInstructions: String,
                           val polyline: Polyline,
                           val startLocation: Location,
                           val travelMode: String) {

    data class Distance(val text: String, val value: Int)
    data class Duration(val text: String, val value: Int)
    data class Location(val lat: Double, val lng: Double)
    data class Polyline(val points: String)
}