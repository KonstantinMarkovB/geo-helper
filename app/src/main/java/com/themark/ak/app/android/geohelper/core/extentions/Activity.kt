package com.themark.ak.app.android.geohelper.core.extentions

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.themark.ak.app.android.geohelper.core.platform.BaseActivity
import com.themark.ak.app.android.geohelper.core.platform.BaseFragment
import kotlinx.android.synthetic.main.activity_location.*


inline fun <reified T : ViewModel> AppCompatActivity.viewMosdel(factory: ViewModelProvider.Factory, body: T.() -> Unit): T {
    val vm = ViewModelProviders.of(this, factory)[T::class.java]
    vm.body()
    return vm
}

val AppCompatActivity.viewContainer: View
    get() {
        return if(coordinatorLayout != null) coordinatorLayout
        else this.mainFragmentContainer.rootView
    }