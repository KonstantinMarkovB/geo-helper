package com.themark.ak.app.android.geohelper.feature.locatr.direction

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface GoogleDirectionApi {
    //json?origin=Toronto&destination=Montreal&key=

    companion object {
        private const val PARAM_ORIGIN = "origin"
        private const val PARAM_DESTINATION = "destination"
        private const val PARAM_KEY = "key"

        private const val DIR_URI = "json?mod=walking"
    }

    @GET(DIR_URI) fun destination(@Query(PARAM_ORIGIN) origin: String,
                                  @Query(PARAM_DESTINATION) destination: String,
                                  @Query(PARAM_KEY) key: String)
            : Call<DirectionAnswerEntry>
}