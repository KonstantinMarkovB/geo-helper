/**
 * Copyright (C) 2018 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.themark.ak.app.android.geohelper.core.di.component

import com.themark.ak.app.android.geohelper.AndroidApplication
import com.themark.ak.app.android.geohelper.core.di.module.ApplicationModule
import com.themark.ak.app.android.geohelper.core.di.module.RetrofitModule
import com.themark.ak.app.android.geohelper.core.di.module.ResourceModule
import com.themark.ak.app.android.geohelper.core.di.viewmodel.ViewModelModule
import com.themark.ak.app.android.geohelper.feature.locatr.search.SearchLocationFragment
import com.themark.ak.app.android.geohelper.feature.locatr.LocationActivity
import com.themark.ak.app.android.geohelper.feature.locatr.LocationFragment
import com.themark.ak.app.android.geohelper.feature.locatr.MapFragment

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    RetrofitModule::class,
    ViewModelModule::class,
    ResourceModule::class
])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(locationFragment: LocationFragment)
    fun inject(locationActivity: LocationActivity)
    fun inject(mapFragment: MapFragment)
    fun inject(searchLocationFragment: SearchLocationFragment)

}
