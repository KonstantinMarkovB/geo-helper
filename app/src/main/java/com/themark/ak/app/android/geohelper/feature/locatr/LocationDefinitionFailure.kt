package com.themark.ak.app.android.geohelper.feature.locatr

import com.themark.ak.app.android.geohelper.core.exception.Failure

class LocationDefinitionFailure : Failure.FeatureFailure() {
}