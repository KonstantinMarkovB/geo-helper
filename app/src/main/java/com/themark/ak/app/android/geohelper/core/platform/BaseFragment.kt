package com.themark.ak.app.android.geohelper.core.platform

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.themark.ak.app.android.geohelper.AndroidApplication
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.di.component.ApplicationComponent
import com.themark.ak.app.android.geohelper.core.extentions.applicationContext
import com.themark.ak.app.android.geohelper.core.extentions.viewContainer
import javax.inject.Inject

/**
 * Base Fragment class with helper methods for handling views.
 *
 * @see Fragment
 */
abstract class BaseFragment : Fragment() {

    /**
     * Get a layout id for setting content view.
     */
    protected abstract fun layoutId(): Int

    /**
     * Gets a [ApplicationComponent] for dependency injection.
     */
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutId(), container, false)

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

    internal fun notify(@StringRes message: Int) =
        Snackbar.make(viewContainer, message, Snackbar.LENGTH_LONG).show()

    internal fun notifyWithAction(@StringRes message: Int, @StringRes actionText: Int, action: () -> Any) {
        val snackBar = Snackbar.make(viewContainer, message, Snackbar.LENGTH_LONG)
        snackBar.setAction(actionText) { action.invoke() }
        snackBar.setActionTextColor(ContextCompat.getColor(applicationContext, R.color.textColor))
        snackBar.show()
    }
}