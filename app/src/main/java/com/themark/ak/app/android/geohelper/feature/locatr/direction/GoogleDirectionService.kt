package com.themark.ak.app.android.geohelper.feature.locatr.direction

import com.themark.ak.app.android.geohelper.core.di.module.RetrofitModule
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class GoogleDirectionService
@Inject constructor(@Named(RetrofitModule.DIRECTIONS) retrofitDir: Retrofit)
    : GoogleDirectionApi {

    private val googleDirectionApi by lazy { retrofitDir.create(GoogleDirectionApi::class.java) }

    override fun destination(origin: String, destination: String, key: String): Call<DirectionAnswerEntry> =
        googleDirectionApi.destination(origin, destination, key)

}