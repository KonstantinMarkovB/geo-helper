package com.themark.ak.app.android.geohelper.feature.locatr.places

import com.themark.ak.app.android.geohelper.core.exception.Failure
import com.themark.ak.app.android.geohelper.core.functional.Either
import com.themark.ak.app.android.geohelper.core.interactor.UseCase
import javax.inject.Inject

class GetPlaces @Inject constructor(
    val repository: PlacesRepository
): UseCase<List<Place>, GetPlaces.Params>() {

    override suspend fun run(params: Params): Either<Failure, List<Place>> =
        repository.getPlaces(params.name, params.location, params.radius)

    class Params(val name: String, val location: String, val radius: String)
}