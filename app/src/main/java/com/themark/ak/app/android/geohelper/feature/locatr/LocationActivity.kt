package com.themark.ak.app.android.geohelper.feature.locatr

import android.Manifest
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.themark.ak.app.android.geohelper.R
import com.themark.ak.app.android.geohelper.core.exception.Failure
import com.themark.ak.app.android.geohelper.core.extentions.*
import com.themark.ak.app.android.geohelper.core.platform.BaseActivity
import com.themark.ak.app.android.geohelper.core.service.PermissionRationalChecker
import com.themark.ak.app.android.geohelper.feature.locatr.search.SearchLocationFragment
import kotlinx.android.synthetic.main.activity_location.*
import kotlinx.android.synthetic.main.activity_location.animation
import kotlinx.android.synthetic.main.fragment_search_location.*
import javax.inject.Inject

class LocationActivity : BaseActivity() {

    companion object{
        private const val REQUEST_ERROR = 0

        private val LOCATION_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        private const val REQUEST_LOCATION_PERMISSIONS = 0
    }

    @Inject lateinit var mapTypseAdapter: MapTypesAdapter

    private lateinit var locationVM: LocationViewModel

    private lateinit var permissionChecker: PermissionRationalChecker

    private var searchDialogFragment: SearchLocationFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appComponent.inject(this)

        if(savedInstanceState == null) {
            addFragment(R.id.mainFragmentContainer, fragment = LocationFragment.newInstance())
            addFragment(R.id.mapFragmentContainer, MapFragment.newInstance())
        }

        locationVM = viewMosdel(viewModelFactory){
            observe(isLoading, ::handleLoading)
            observe(mapTypeVisibility, ::handleMapTypesVisibility)
            observe(mapTypes, ::handleMapTypes)
            observe(isMapLoaded, ::handleMapIsLoaded)
            observe(searchDialogVisibility, ::handleSearchDialogVisibility)
            processFailure(failure, ::handleFailure)
        }

        permissionChecker = PermissionRationalChecker
            .Builder(this, REQUEST_LOCATION_PERMISSIONS)
            .setPermissions(LOCATION_PERMISSIONS)
            .setRationaleMassage(R.string.locatr_permissions_description)
            .create()

        initUI()
    }


    private fun initUI(){
        currentLocation_btn.setOnClickListener {
           loadCurrentLocation()
        }

        mapTypesRecyclerView.layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        mapTypesRecyclerView.adapter = mapTypseAdapter
        mapTypseAdapter.clickListener = { locationVM.changeMapType(it) }

        closeMapTypeContainer.setOnClickListener { locationVM.hideMapTypes() }
        mapType_btn.setOnClickListener { locationVM.showMapTypes() }
    }

    override fun onResume() {
        super.onResume()

        checkApiAvailability()
//        locationVM.isMapLoaded.observe(this, Observer(::handleMapIsLoaded))

    }

    private fun handleSearchDialogVisibility(visibility: Boolean?){
        when(visibility){
            true -> {
                currentLocation_btn.invisible()
                searchLocationFragment.visible()
                if(searchDialogFragment == null) {
                    searchDialogFragment = SearchLocationFragment.newInstance()
                    addFragment(R.id.searchLocationFragment, searchDialogFragment!!)
                }
            }
            false, null -> {
                searchLocationFragment.invisible()
                currentLocation_btn.visible()
            }

        }


    }

    private fun checkApiAvailability(){
        val apiAvailability = GoogleApiAvailability.getInstance()
        val errorCode = apiAvailability.isGooglePlayServicesAvailable(this)

        if(errorCode != ConnectionResult.SUCCESS){
            val errorDialog = apiAvailability.getErrorDialog(
                this, errorCode,
                REQUEST_ERROR) {
                finish()
            }
            errorDialog.show()
        }
    }

    private fun handleLoading(isLoading: Boolean?){
        isLoading?.let {
            if(it){
                animation.visible()
            } else{
                animation.invisible()
            }
        }
    }

    private fun handleMapTypes(list: List<MapTypeView>?){
        list?.let { mapTypseAdapter.collection = list }
    }

    private fun handleMapTypesVisibility(visibility: Boolean?){
        when(visibility){
            true -> {
                closeMapTypeContainer.visible()
                mapTypeContainer.visible()
            }
            else -> {
                closeMapTypeContainer.invisible()
                mapTypeContainer.invisible()
            }
        }
    }

    private fun handleMapIsLoaded(isLoaded: Boolean?){
        when(isLoaded){
            true -> loadCurrentLocation()
        }
    }

    private fun handleFailure(failure: Failure?) : Boolean{
        when(failure){
            is LocationDefinitionFailure -> {
                notify(R.string.location_failure)
                return true
            }
        }

        Log.d("TEST", "$failure")
        return false
    }

    private fun loadCurrentLocation(){
        when {
            permissionChecker.hasPermissions() -> locationVM.showCurrentLocation()
            else -> permissionChecker.show()
        }
    }

    override fun onStart() {
        super.onStart()
        locationVM.onStart()
    }

    override fun onStop() {
        super.onStop()
        locationVM.onStop()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            permissionChecker.requestCode ->
                if(permissionChecker.hasPermissions()) {
                    locationVM.showCurrentLocation()
                }
            else ->
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        }
    }

}