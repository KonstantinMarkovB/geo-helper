package com.themark.ak.app.android.geohelper.feature.locatr.places

import com.themark.ak.app.android.geohelper.core.di.module.RetrofitModule
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class GooglePlacesService
@Inject constructor(@Named(RetrofitModule.PLACES) val retrofit: Retrofit) :
    GooglePlacesApi {

    private val googlePlacesApi by lazy { retrofit.create(GooglePlacesApi::class.java) }

    override fun places(name: String, location: String, radius: String, key: String): Call<GooglePlacesAnswerEntry> =
        googlePlacesApi.places(name, location, radius, key)
}